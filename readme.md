Install and start with: `npm ci && npm start`

Open browser and navigate to 
`http://localhost:3000/SYD/SFJ` (to get ordinary route)
or
`http://localhost:3000/PMY/SYD` (to get byLand route)

Note that you may have to wait, because

a) Data is fetched in background

b) Graph from-to-lookup is build on demand and cached -> every consequent call to any route will be much-much faster.
    I decided not to warm up anything in advance...

Ideas:
1) BFS with pruning by shortest route + Dijkstra optimizations
2) This is synchronous operation that could block - ok for test assignment, may be not ok for real service

To run tests run: `npm test`
