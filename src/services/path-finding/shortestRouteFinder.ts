import { Airport, Path, ProcessedRoute } from "../../typings";
import { memoize } from "lodash";

class ShortestRouteFinder {
  private findRoutesFrom = memoize((from: Airport) => this.routes.filter((it) => it.from === from));

  constructor(private routes: ProcessedRoute[]) {
    this.routes = routes.filter((route) => route.from && route.to);
  }

  findShortestRoute(from: Airport, to: Airport) {
    let routes: Path[];
    const shortestVisitedDestinations = new Map<string, Path>();
    routes = this.calculateFirstLevelRoutes(from, to, shortestVisitedDestinations)!;
    routes = this.calculateIntermediateLevelRoutes(routes, to, shortestVisitedDestinations);
    routes = this.calculateIntermediateLevelRoutes(routes, to, shortestVisitedDestinations);
    routes = this.calculateIntermediateLevelRoutes(routes, to, shortestVisitedDestinations);
    return this.calculateLastLevelRoutes(routes, to, shortestVisitedDestinations);
  }

  private calculateFirstLevelRoutes(from: Airport, to: Airport, shortestVisitedDestinations: Map<string, Path>) {
    let routesFirstLevel: Path[] = [];
    for (const route of this.findRoutesFrom(from)) {
      const path = {
        path: [route.from, route.to],
        distance: route.distance,
        last: route.to,
        byLand: route.byLand ? route : undefined,
      };
      // fill in current shortest routes yet so far for dijkstra optimization in intermediate loops
      shortestVisitedDestinations.set(route.to.code, path);
      if (to.code === route.to.code) {
        // direct flight found -> it is the shortest one
        break;
      }
      routesFirstLevel.push(path);
    }
    return routesFirstLevel;
  }

  private calculateIntermediateLevelRoutes(
    routesFromPreviousLevel: Path[],
    to: Airport,
    shortestVisitedDestinations: Map<string, Path>
  ) {
    let routesForNewLevel: Path[] = [];
    for (const previousLevelRoute of routesFromPreviousLevel) {
      // if shortest found route is shorter than current then do not watch further
      if (
        shortestVisitedDestinations.get(to.code) &&
        previousLevelRoute.distance > shortestVisitedDestinations.get(to.code)!.distance
      ) {
        continue;
      }
      const previousLevelPath = previousLevelRoute.path;
      for (const route of this.findRoutesFrom(previousLevelRoute.last)) {
        const distance = previousLevelRoute.distance + route.distance;
        // if shortest route is shorter than current new route then do not watch further
        if (!ShortestRouteFinder.isShorter(shortestVisitedDestinations.get(to.code), distance)) {
          // break because this.findRoutesFrom(previousLevelRoute.last) returns ordered routes
          // any next going route will be longer
          break;
        }
        if (previousLevelRoute.path.includes(route.to)) {
          // throw away cycles TLL-HEL-TLL -> this might be not needed due to Dijkstra below
          continue;
        }
        // Do not watch further if more than one hop by land was done already
        if (previousLevelRoute.byLand && route.byLand) {
          continue;
        }
        const path = {
          path: previousLevelPath.concat(route.to),
          distance,
          last: route.to,
          byLand: ShortestRouteFinder.getByLandRoute(previousLevelRoute, route),
        };
        if (to.code === route.to.code) {
          // new shortest route found
          shortestVisitedDestinations.set(route.to.code, path);
          // trim collected routes to be less than our new shortest one
          routesForNewLevel = routesForNewLevel.filter((it) => it.distance < distance);
          continue;
        }
        // Apply Dijkstra idea - if you already have shortest route to some point
        // there is no idea to watch longer route with same point any further
        const alreadyVisited = shortestVisitedDestinations.get(route.to.code);
        if (!alreadyVisited || alreadyVisited.distance > distance) {
          shortestVisitedDestinations.set(route.to.code, path);
          routesForNewLevel.push(path);
        }
      }
    }
    return routesForNewLevel;
  }

  private calculateLastLevelRoutes(
    routesFromPreviousLevel: Path[],
    to: Airport,
    shortestVisitedDestinations: Map<string, Path>
  ) {
    for (const previousLevelRoute of routesFromPreviousLevel) {
      const previousLevelPath = previousLevelRoute.path;
      for (const route of this.findRoutesFrom(previousLevelRoute.last)) {
        const distance = previousLevelRoute.distance + route.distance;
        // if shortest route is shorter than current new route then do not watch further
        if (!ShortestRouteFinder.isShorter(shortestVisitedDestinations.get(to.code), distance)) {
          // distance is ordered, there will be nothing more in this loop
          break;
        }
        // in last loop look only for destination
        if (to.code !== route.to.code) {
          continue;
        }
        // Do not watch further if by land hop was used
        if (previousLevelRoute.byLand && route.byLand) {
          continue;
        }
        // Do not watch further if by land hop was not used but last route is not by land route (LIMIT legs to 4 + 1 starting point)
        if (previousLevelRoute.path.length === 5 && !previousLevelRoute.byLand && !route.byLand) {
          continue;
        }
        // update shortest route
        shortestVisitedDestinations.set(to.code, {
          path: previousLevelPath.concat(route.to),
          distance,
          last: route.to,
          byLand: ShortestRouteFinder.getByLandRoute(previousLevelRoute, route),
        });
      }
    }
    return shortestVisitedDestinations.get(to.code);
  }

  private static getByLandRoute(previousLevelRoute: Path, route: ProcessedRoute) {
    return previousLevelRoute.byLand || (route.byLand ? route : undefined);
  }

  private static isShorter(shortestRoute: Path | undefined, distance: number) {
    return !shortestRoute || shortestRoute!.distance > distance;
  }
}

export default ShortestRouteFinder;
