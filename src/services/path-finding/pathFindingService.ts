import { Airport, Route, ProcessedRoute } from "../../typings";
import { fetchData } from "./fetchData";
import ShortestRouteFinder from "./shortestRouteFinder";
import { createRoutesFrom } from "./routesBuilder";

class PathFindingService {
  private routes!: ProcessedRoute[];
  private airports!: Map<string, Airport>;
  private initialized: boolean = false;
  private shortestRouteFinder?: ShortestRouteFinder;

  async initService(airports?: Map<string, Airport>, routes?: Route[]) {
    if (!airports && !routes) {
      [airports, routes] = await fetchData();
    }
    this.airports = airports!;
    this.routes = createRoutesFrom(routes!, this.airports);
    this.shortestRouteFinder = new ShortestRouteFinder(this.routes);
    this.initialized = true;
  }

  isReady() {
    return this.initialized;
  }

  findAirport(code: string) {
    return this.airports.get(code);
  }

  findPath(from: Airport, to: Airport) {
    const shortestRoute = this.shortestRouteFinder!.findShortestRoute(from, to);
    if (!shortestRoute) {
      throw new Error("No routes with 3 layovers found");
    }
    return shortestRoute;
  }
}

export default PathFindingService;
