import { Airport, ProcessedRoute, Route } from "../../typings";
import { getDistance, isPointWithinRadius } from "geolib";
import { sortBy } from "lodash";

export function createRoutesFrom(routes: Route[], airports: Map<string, Airport>) {
  const airRoutes = calculateRouteDistance(routes, airports);
  const landRoutes = createLandRoutes(orderedByLatitude(airports));
  return orderByDistanceAsc(airRoutes.concat(landRoutes));
}

function calculateRouteDistance(routes: Route[], airports: Map<string, Airport>) {
  const processedRoutes: ProcessedRoute[] = [];
  routes.map((route) => {
    const { from, to } = route;
    const fromAirport = airports.get(from)!;
    const toAirPort = airports.get(to)!;
    if (!toAirPort || !fromAirport) {
      return;
    }
    processedRoutes.push({
      from: fromAirport,
      to: toAirPort,
      distance: calculateDistance(fromAirport, toAirPort),
      byLand: false,
    });
  });
  return processedRoutes;
}

function createLandRoutes(airports: Airport[]) {
  const landRoutes: ProcessedRoute[] = [];
  for (let i = 0; i < airports.length; i++) {
    const from = airports[i];
    // latitude 1 is approx more than 100km
    const distantFromLat = from.latitude + 1;
    // Watch only subsequent routes
    for (let k = i + 1; k < airports.length; k++) {
      const to = airports[k];
      // if is far away then break, they are ordered no sense to watch further
      if (to.latitude > distantFromLat) {
        break;
      }
      if (
        isPointWithinRadius(
          { latitude: to.latitude, longitude: to.longitude },
          {
            latitude: from.latitude,
            longitude: from.longitude,
          },
          100000
        )
      ) {
        const distance = calculateDistance(from, to);
        landRoutes.push({
          from,
          to,
          distance: distance,
          byLand: true,
        });
        // Create vice versa route
        landRoutes.push({
          to: from,
          from: to,
          distance: distance,
          byLand: true,
        });
      }
    }
  }
  return landRoutes;
}

function orderedByLatitude(airports: Map<string, Airport>): Airport[] {
  return sortBy(Array.from(airports.values()), ["latitude"]);
}

function orderByDistanceAsc(routes: ProcessedRoute[]): ProcessedRoute[] {
  return sortBy(routes, ["distance"]);
}

function calculateDistance(fromAirport: Airport, toAirPort: Airport) {
  return getDistance(
    { latitude: fromAirport.latitude, longitude: fromAirport.longitude },
    { latitude: toAirPort.latitude, longitude: toAirPort.longitude }
  );
}
