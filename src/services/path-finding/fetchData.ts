import { toNumber } from "lodash";
import got from "got";
import Papa from "papaparse";
import { Airport, Route } from "../../typings";

const AIRPORT_URL = "https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat";
const ROUTE_URL = "https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat";

export async function fetchData(): Promise<[Map<string, Airport>, Route[]]> {
  return Promise.all([fetchAirports(), fetchRoutes()]);
}

async function fetchAirports(): Promise<Map<string, Airport>> {
  const airportsRows = await fetchAndParse(AIRPORT_URL);
  const airportsMap = new Map<string, Airport>();
  airportsRows.forEach((row) => {
    // 1,"Goroka Airport","Goroka","Papua New Guinea","GKA","AYGA",-6.081689834590001,145.391998291,5282,10,"U","Pacific/Port_Moresby","airport","OurAirports"
    const code = row[4];
    if (!code) {
      return;
    }
    airportsMap.set(code, {
      code,
      name: row[1],
      latitude: toNumber(row[6]),
      longitude: toNumber(row[7]),
    });
  });
  return airportsMap;
}

async function fetchRoutes(): Promise<Route[]> {
  const routesRows = await fetchAndParse(ROUTE_URL);
  const allowOnlyDirectFlight = (row: string[]) => row[7] === "0";
  return routesRows.filter(allowOnlyDirectFlight).map((row) => {
    //2B,410,AER,2965,KZN,2990,,0,CR2
    const from = row[2];
    const to = row[4];
    return { from, to };
  });
}

async function fetchAndParse(url: string) {
  const csv = (await got.get(url)).body;
  return Papa.parse(csv).data as string[][];
}
