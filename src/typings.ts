export type Airport = {
  name: string;
  code: string;
  latitude: number;
  longitude: number;
};

export type Route = {
  from: string;
  to: string;
};

export type ProcessedRoute = {
  byLand?: boolean;
  distance: number;
  from: Airport;
  to: Airport;
};

export type Path = {
  path: Airport[];
  distance: number;
  last: Airport;
  byLand: ProcessedRoute | undefined;
};

export type NearestFlightResponse = {
  from: Airport;
  to: Airport;
  distance: number;
  path: Airport[];
  byLand: ProcessedRoute | false;
};
