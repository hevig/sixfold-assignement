import Koa from "koa";
import logger from "koa-logger";
import Router from "@koa/router";
import { calculateNearestFlight } from "./routes/calculateNearestFlight";

const app = new Koa();
const router = new Router();
app.use(logger());

router.get("/:from/:to", async (ctx) => {
  try {
    const { from, to } = ctx.params;
    ctx.body = await calculateNearestFlight(from, to);
  } catch (e) {
    ctx.status = 500;
    ctx.body = {
      message: e.message,
    };
  }
});

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(+process.env.PORT! || 3000, "0.0.0.0");

export default app;
