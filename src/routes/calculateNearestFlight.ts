import { NearestFlightResponse } from "../typings";
import PathFindingService from "../services/path-finding/pathFindingService";

const pathFindingService = new PathFindingService();
pathFindingService.initService();

export async function calculateNearestFlight(fromCode: string, toCode: string): Promise<NearestFlightResponse> {
  if (!pathFindingService.isReady()) {
    throw new Error("Service not yet initialized, please retry after a while...");
  }
  const from = pathFindingService.findAirport(fromCode);
  if (!from) {
    throw new Error(`Airport with code ${fromCode} was not found`);
  }
  const to = pathFindingService.findAirport(toCode);
  if (!to) {
    throw new Error(`Airport with code ${toCode} was not found`);
  }
  const path = pathFindingService.findPath(from, to);

  return {
    from,
    to,
    byLand: path.byLand || false,
    distance: path.distance,
    path: path.path,
  };
}
