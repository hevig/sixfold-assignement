import test from "ava";
import { Airport } from "../src/typings";
import { createRoutesFrom } from "../src/services/path-finding/routesBuilder";

test("test sample requirements", async (t) => {
  const a = { code: "a", name: "a", latitude: 0, longitude: 0 };
  const b = { code: "b", name: "b", latitude: 0, longitude: 0.5 };
  const routes = createRoutesFrom(
    [],
    new Map<string, Airport>([
      ["a", a],
      ["b", b],
    ])
  );
  t.deepEqual(routes, [
    { from: a, to: b, distance: 55660, byLand: true },
    { from: b, to: a, distance: 55660, byLand: true },
  ]);
});

test("test routes not built", async (t) => {
  const a = { code: "a", name: "a", latitude: 0, longitude: 0 };
  const b = { code: "b", name: "b", latitude: 0, longitude: 1 };
  const routes = createRoutesFrom(
    [],
    new Map<string, Airport>([
      ["a", a],
      ["b", b],
    ])
  );
  t.deepEqual(routes, []);
});
