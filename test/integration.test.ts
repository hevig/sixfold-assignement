import test from "ava";
import request from "supertest";
import app from "./../src/index";

// 10 seconds is enough, for simplicity I will leave this naive implementation
function waitForInitialization(delay = 5000) {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
}

test("http://localhost:3000/SYD/SFJ", async (t) => {
  try {
    await waitForInitialization();
    const response = await request(app.callback()).get("/SYD/SFJ");
    t.deepEqual(response.body, {
      from: {
        code: "SYD",
        name: "Sydney Kingsford Smith International Airport",
        latitude: -33.94609832763672,
        longitude: 151.177001953125,
      },
      to: {
        code: "SFJ",
        name: "Kangerlussuaq Airport",
        latitude: 67.0122218992,
        longitude: -50.7116031647,
      },
      byLand: false,
      distance: 19521803,
      path: [
        {
          code: "SYD",
          name: "Sydney Kingsford Smith International Airport",
          latitude: -33.94609832763672,
          longitude: 151.177001953125,
        },
        {
          code: "HKG",
          name: "Hong Kong International Airport",
          latitude: 22.308901,
          longitude: 113.915001,
        },
        {
          code: "SVO",
          name: "Sheremetyevo International Airport",
          latitude: 55.972599,
          longitude: 37.4146,
        },
        {
          code: "CPH",
          name: "Copenhagen Kastrup Airport",
          latitude: 55.617900848389,
          longitude: 12.656000137329,
        },
        { code: "SFJ", name: "Kangerlussuaq Airport", latitude: 67.0122218992, longitude: -50.7116031647 },
      ],
    });
  } catch (e) {
    t.fail(e);
  }
});

test("http://localhost:3000/PMY/SYD", async (t) => {
  try {
    await waitForInitialization();
    const response = await request(app.callback()).get("/PMY/SYD");
    t.deepEqual(response.body, {
      from: { code: "PMY", name: "El Tehuelche Airport", latitude: -42.7592, longitude: -65.1027 },
      to: {
        code: "SYD",
        name: "Sydney Kingsford Smith International Airport",
        latitude: -33.94609832763672,
        longitude: 151.177001953125,
      },
      byLand: {
        to: {
          code: "REL",
          name: "Almirante Marco Andres Zar Airport",
          latitude: -43.2105,
          longitude: -65.2703,
        },
        from: {
          code: "PMY",
          name: "El Tehuelche Airport",
          latitude: -42.7592,
          longitude: -65.1027,
        },
        distance: 52059,
        byLand: true,
      },
      distance: 13087580,
      path: [
        {
          code: "PMY",
          name: "El Tehuelche Airport",
          latitude: -42.7592,
          longitude: -65.1027,
        },
        {
          code: "REL",
          name: "Almirante Marco Andres Zar Airport",
          latitude: -43.2105,
          longitude: -65.2703,
        },
        {
          code: "BRC",
          name: "San Carlos De Bariloche Airport",
          latitude: -41.151199,
          longitude: -71.157501,
        },
        {
          code: "MDZ",
          name: "El Plumerillo Airport",
          latitude: -32.8316993713,
          longitude: -68.7929000854,
        },
        {
          code: "SCL",
          name: "Comodoro Arturo Merino Benítez International Airport",
          latitude: -33.393001556396484,
          longitude: -70.78579711914062,
        },
        {
          code: "SYD",
          name: "Sydney Kingsford Smith International Airport",
          latitude: -33.94609832763672,
          longitude: 151.177001953125,
        },
      ],
    });
  } catch (e) {
    t.fail(e);
  }
});
