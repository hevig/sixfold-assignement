import test from "ava";
import PathFindingService from "../src/services/path-finding/pathFindingService";
import { Airport } from "../src/typings";

const A = { code: "A", longitude: 0, latitude: 0, name: "A" };
const B = { code: "B", longitude: 5, latitude: 5, name: "B" };

test("test sample requirements", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  airports.set("1", { code: "1", longitude: 1, latitude: 1, name: "1" });
  airports.set("2", { code: "2", longitude: 2, latitude: 2, name: "2" });
  airports.set("3", { code: "3", longitude: 3, latitude: 3, name: "3" });
  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    { from: "2", to: "3" },
    { from: "3", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [
    A,
    { code: "1", longitude: 1, latitude: 1, name: "1" },
    { code: "2", longitude: 2, latitude: 2, name: "2" },
    { code: "3", longitude: 3, latitude: 3, name: "3" },
    B,
  ]);
});

test("test sample requirements with 4 legs that shall never happen", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  airports.set("1", { code: "1", longitude: 1, latitude: 1, name: "1" });
  airports.set("2", { code: "2", longitude: 2, latitude: 2, name: "2" });
  airports.set("3", { code: "3", longitude: 3, latitude: 3, name: "3" });
  airports.set("4", { code: "4", longitude: 4, latitude: 4, name: "4" });
  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    { from: "2", to: "3" },
    { from: "3", to: "4" },
    { from: "4", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  try {
    pathFindingService.findPath(A, B);
  } catch (e) {
    t.is(e.message, "No routes with 3 layovers found");
  }
});

test("pathfinder should choose longer route with less legs, even though shorter distance route with 4 legs exists", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);

  airports.set("a1", { code: "a1", longitude: 1, latitude: 1, name: "a1" });
  airports.set("a2", { code: "a2", longitude: 2, latitude: 2, name: "a2" });
  airports.set("a3", { code: "a3", longitude: 3, latitude: 3, name: "a3" });
  airports.set("a4", { code: "a4", longitude: 4, latitude: 4, name: "a4" });

  airports.set("x1", { code: "x1", longitude: 10, latitude: 10, name: "x1" });

  airports.set("B", B);

  const routes = [
    { from: "A", to: "a1" },
    { from: "a1", to: "a2" },
    { from: "a2", to: "a3" },
    { from: "a3", to: "a4" },
    { from: "a4", to: "B" },
    { from: "A", to: "x1" },
    { from: "x1", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);
  t.deepEqual(path.path, [A, { code: "x1", longitude: 10, latitude: 10, name: "x1" }, B]);
});

test("pathfinder should be ok with loops", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);

  airports.set("a1", { code: "a1", longitude: 1, latitude: 1, name: "a1" });
  airports.set("a2", { code: "a2", longitude: 2, latitude: 2, name: "a2" });

  airports.set("B", B);

  const routes = [
    { from: "A", to: "a1" },
    { from: "a1", to: "a2" },
    { from: "a2", to: "a1" },
    { from: "a2", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);
  t.deepEqual(path.path, [
    A,
    { code: "a1", longitude: 1, latitude: 1, name: "a1" },
    { code: "a2", longitude: 2, latitude: 2, name: "a2" },
    B,
  ]);
});

test("pathfinder should find shortest path", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);

  airports.set("a1", { code: "a1", longitude: 1, latitude: 1, name: "a1" });
  airports.set("a2", { code: "a2", longitude: 2, latitude: 2, name: "a2" });
  airports.set("a3", { code: "a3", longitude: 3, latitude: 3, name: "a3" });

  airports.set("B", B);

  const routes = [
    { from: "A", to: "a1" },
    { from: "a1", to: "a3" },
    { from: "a3", to: "B" },
    { from: "A", to: "a2" },
    { from: "a2", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [A, { code: "a2", longitude: 2, latitude: 2, name: "a2" }, B]);
});

test("should find shortest path through land", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  const one = { code: "1", longitude: 1, latitude: 1, name: "1" };
  airports.set("1", one);
  airports.set("2", { code: "2", longitude: 20, latitude: 20, name: "2" });
  airports.set("3", { code: "3", longitude: 3, latitude: 3, name: "3" });

  const oneA = { code: "1a", longitude: 1.1, latitude: 1.1, name: "1a" };
  airports.set("1a", oneA);

  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    { from: "2", to: "3" },
    { from: "3", to: "B" },
    { from: "1a", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [
    A,
    { code: "1", longitude: 1, latitude: 1, name: "1" },
    { code: "1a", longitude: 1.1, latitude: 1.1, name: "1a" },
    B,
  ]);
  t.deepEqual(path.byLand, { from: one, to: oneA, distance: 15742, byLand: true });
});

test("should not go through land with 2 land stops", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  airports.set("1", { code: "1", longitude: 1, latitude: 1, name: "1" });
  airports.set("2", { code: "2", longitude: 20, latitude: 20, name: "2" });
  airports.set("3", { code: "3", longitude: 3, latitude: 3, name: "3" });

  airports.set("1a", { code: "1a", longitude: 1.1, latitude: 1.1, name: "1a" });
  airports.set("1b", { code: "1b", longitude: 1.1, latitude: 1.9, name: "1b" });

  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    { from: "2", to: "3" },
    { from: "3", to: "B" },
    { from: "1b", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [
    A,
    { code: "1", longitude: 1, latitude: 1, name: "1" },
    { code: "2", longitude: 20, latitude: 20, name: "2" },
    { code: "3", longitude: 3, latitude: 3, name: "3" },
    B,
  ]);
  t.deepEqual(path.byLand, undefined);
});

test("should go 4 stops with one 1 land stop", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  airports.set("1", { code: "1", longitude: 1, latitude: 1, name: "1" });
  airports.set("2", { code: "2", longitude: 2, latitude: 2, name: "2" });
  airports.set("3", { code: "3", longitude: 3, latitude: 3, name: "3" });
  airports.set("4", { code: "4", longitude: 4.9, latitude: 4.9, name: "4" });
  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    { from: "2", to: "3" },
    { from: "3", to: "4" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [
    A,
    { code: "1", longitude: 1, latitude: 1, name: "1" },
    { code: "2", longitude: 2, latitude: 2, name: "2" },
    { code: "3", longitude: 3, latitude: 3, name: "3" },
    { code: "4", longitude: 4.9, latitude: 4.9, name: "4" },
    B,
  ]);
  t.deepEqual(path.byLand, {
    from: { code: "4", longitude: 4.9, latitude: 4.9, name: "4" },
    to: { code: "B", longitude: 5, latitude: 5, name: "B" },
    distance: 15714,
    byLand: true,
  });
});

test("should go 4 stops with one 1 land stop v2", async (t) => {
  const pathFindingService = new PathFindingService();
  const airports = new Map<string, Airport>();
  airports.set("A", A);
  airports.set("1", { code: "1", longitude: 1, latitude: 1, name: "1" });
  airports.set("2", { code: "2", longitude: 2, latitude: 2, name: "2" });
  airports.set("3", { code: "3", longitude: 2.1, latitude: 2.1, name: "3" });
  airports.set("4", { code: "4", longitude: 4, latitude: 4, name: "4" });
  airports.set("B", B);

  const routes = [
    { from: "A", to: "1" },
    { from: "1", to: "2" },
    // { from: "2", to: "3" },
    { from: "3", to: "4" },
    { from: "4", to: "B" },
  ];

  await pathFindingService.initService(airports, routes);

  const path = pathFindingService.findPath(A, B);

  t.deepEqual(path.path, [
    A,
    { code: "1", longitude: 1, latitude: 1, name: "1" },
    { code: "2", longitude: 2, latitude: 2, name: "2" },
    { code: "3", longitude: 2.1, latitude: 2.1, name: "3" },
    { code: "4", longitude: 4, latitude: 4, name: "4" },
    B,
  ]);
  t.deepEqual(path.byLand, {
    from: { code: "2", longitude: 2, latitude: 2, name: "2" },
    to: { code: "3", longitude: 2.1, latitude: 2.1, name: "3" },
    distance: 15738,
    byLand: true,
  });
});
